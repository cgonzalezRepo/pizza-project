<?php

namespace App\Repository;

use App\Entity\Recipe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Recipe|null find($id, $lockMode = null, $lockVersion = null)
 * @method Recipe|null findOneBy(array $criteria, array $orderBy = null)
 * @method Recipe[]    findAll()
 * @method Recipe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecipeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Recipe::class);
    }

    // /**
    //  * @return Recipe[] Returns an array of Recipe objects
    //  */
    public function findAllRecipes()
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT pizza.name AS pizzaName, ingredient.name AS ingredientName, ingredient.price AS ingredientPrice, ingredient.id AS ingredientId
        FROM recipe
        JOIN pizza
        JOIN ingredient
        ON recipe.pizza_id_id = pizza.id AND recipe.ingredient_id_id = ingredient.id;
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute([]);

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAll();
    }

    // /**
    //  * @return Recipe[] Returns an array of Recipe objects
    //  */
    public function findRecipeById($id)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT pizza.name AS pizzaName, ingredient.name AS ingredientName, ingredient.price AS ingredientPrice, ingredient.id AS ingredientId
        FROM recipe
        JOIN pizza
        JOIN ingredient
        ON recipe.pizza_id_id = pizza.id AND recipe.ingredient_id_id = ingredient.id
        WHERE recipe.pizza_id_id = ' . $id;

        $stmt = $conn->prepare($sql);
        $stmt->execute([]);

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAll();
    }
}
