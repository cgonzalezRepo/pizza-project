<?php

namespace App\Controller;

use App\Entity\Pizza;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PizzaController extends AbstractController
{
    /**
     * @Route("/pizza", name="pizza")
     */
    public function index()
    {
        return $this->render('pizza/index.html.twig', [
            'pizzas' => $this->showAllPizzas(),
        ]);
    }

    public function showAllPizzas()
    {
        $repository = $this->getDoctrine()->getRepository(Pizza::class);

        // look for *all* Pizzas objects
        $pizzas = $repository->findAll();

        return $pizzas;
    }
}
