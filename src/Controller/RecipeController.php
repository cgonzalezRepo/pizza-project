<?php

namespace App\Controller;

use App\Entity\Ingredient;
use App\Entity\Pizza;
use App\Entity\Recipe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class RecipeController extends AbstractController
{
    /**
     * @Route("/recipe", name="recipe")
     */
    public function index()
    {
        return $this->render('recipe/index.html.twig', [
            'recipes' => $this->getAllRecipes(),
        ]);
    }

    /**
     * @Route("/recipe/{id}", name="recipes")
     */
    public function showRecipeById($id)
    {
        return $this->render('recipe/index.html.twig', [
            'recipes' => $this->getRecipeById($id),
            'pizza' => $this->getPizzaName($id),
            'ingredients' => $this->getAllIngredients(),
        ]);
    }

    public function getRecipeById($id)
    {
        $repository = $this->getDoctrine()->getRepository(Recipe::class);
        $recipes = $repository->findRecipeById($id);

        $prettyRecipeOutput = array();
        foreach ($recipes as $recipe) {
            $prettyRecipeOutput[$recipe["pizzaName"]]["Ingredients"][$recipe["ingredientName"]] = $recipe["ingredientPrice"];
        }

        return $prettyRecipeOutput;
    }

    public function getAllRecipes()
    {
        $repository = $this->getDoctrine()->getRepository(Recipe::class);
        $recipes = $repository->findAllRecipes();

        $prettyRecipeOutput = array();
        foreach ($recipes as $recipe) {
            $prettyRecipeOutput[$recipe["pizzaName"]]["Ingredients"][$recipe["ingredientName"]] = $recipe["ingredientPrice"];
        }

        return $prettyRecipeOutput;
    }

    public function getPizzaName($id)
    {
        $repository = $this->getDoctrine()->getRepository(Pizza::class);
        $pizza = $repository->find($id);

        return $pizza;
    }

    public function getAllIngredients()
    {
        $repository = $this->getDoctrine()->getRepository(Ingredient::class);
        $ingredients = $repository->findAll();

        return $ingredients;
    }
}
