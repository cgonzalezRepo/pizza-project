<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RecipeRepository")
 */
class Recipe
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ingredient")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ingredient_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Pizza")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pizza_id;

    public function getId():  ? int
    {
        return $this->id;
    }

    public function getIngredientId() :  ? ingredient
    {
        return $this->ingredient_id;
    }

    public function setIngredientId( ? ingredient $ingredient_id) : self
    {
        $this->ingredient_id = $ingredient_id;

        return $this;
    }

    public function getPizzaId() :  ? pizza
    {
        return $this->pizza_id;
    }

    public function setPizzaId( ? pizza $pizza_id) : self
    {
        $this->pizza_id = $pizza_id;

        return $this;
    }
}
