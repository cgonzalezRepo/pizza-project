-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 03-02-2019 a las 15:30:04
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pizza-project`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingredient`
--

CREATE TABLE `ingredient` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `ingredient`
--

INSERT INTO `ingredient` (`id`, `name`, `price`) VALUES
(1, 'tomato', 0.5),
(2, 'sliced mushrooms', 0.5),
(3, 'feta cheese', 1),
(4, 'sausages', 1),
(5, 'sliced onion', 1),
(6, 'mozzarella cheese', 1),
(7, 'oregano', 0.3),
(8, 'bacon', 1),
(9, 'ham', 2),
(10, 'black olive', 0.2),
(11, 'green olive', 0.2),
(12, 'pineapple', 0.3),
(13, 'anchovy', 1),
(14, 'artichoke', 0.5),
(15, 'egg', 0.5),
(16, 'salami', 1),
(17, 'peas', 0.3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20190202144636', '2019-02-02 14:49:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pizza`
--

CREATE TABLE `pizza` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pizza`
--

INSERT INTO `pizza` (`id`, `name`, `description`) VALUES
(1, 'Capricciosa', 'Pizza capricciosa is a style of pizza in Italian cuisine prepared with mozzarella cheese, Italian baked ham, mushroom, artichoke, anchovies and tomato'),
(2, 'Quattro Stagioni', 'Pizza quattro stagioni is typically prepared using artichokes, tomatoes or basil, mushrooms and ham or prosciutto, or olives.'),
(3, 'Hawaiian', 'Hawaiian pizza is a pizza topped with tomato sauce, cheese, pineapple, and back bacon or ham. Some versions may include peppers, mushrooms, bacon or pepperoni.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recipe`
--

CREATE TABLE `recipe` (
  `id` int(11) NOT NULL,
  `ingredient_id_id` int(11) NOT NULL,
  `pizza_id_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `recipe`
--

INSERT INTO `recipe` (`id`, `ingredient_id_id`, `pizza_id_id`) VALUES
(1, 1, 1),
(2, 6, 1),
(3, 2, 1),
(4, 9, 1),
(5, 11, 1),
(6, 4, 1),
(7, 14, 1),
(8, 1, 3),
(9, 6, 3),
(10, 9, 3),
(11, 12, 3),
(12, 1, 2),
(13, 6, 2),
(14, 9, 2),
(15, 10, 2),
(16, 2, 2),
(17, 14, 2),
(18, 15, 2),
(19, 16, 2),
(20, 17, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indices de la tabla `pizza`
--
ALTER TABLE `pizza`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `recipe`
--
ALTER TABLE `recipe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_DA88B1376676F996` (`ingredient_id_id`),
  ADD KEY `IDX_DA88B13789359C8F` (`pizza_id_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `pizza`
--
ALTER TABLE `pizza`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `recipe`
--
ALTER TABLE `recipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `recipe`
--
ALTER TABLE `recipe`
  ADD CONSTRAINT `FK_DA88B1376676F996` FOREIGN KEY (`ingredient_id_id`) REFERENCES `ingredient` (`id`),
  ADD CONSTRAINT `FK_DA88B13789359C8F` FOREIGN KEY (`pizza_id_id`) REFERENCES `pizza` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
